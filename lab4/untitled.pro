TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    libword.cpp \
    main.cpp

HEADERS += \
    libword.h \
    libchange.h
