
QT += core
QT -= gui

CONFIG += c++11

TARGET = Programm7_2
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app
SOURCES += main.cpp

DISTFILES += \
    ../build-untitled3-Desktop_Qt_5_9_1_MinGW_32bit-Debug/input.txt
