#ifndef MATRIX
#define MATRIX

class Numbers
{
private:
    int Count;
    int arr[100][100];
public:
    void setMatrix (int Count);
    void printMatrix (int *arr);
    void workMatrix (int Count);


    int getSum(void);
    int getDifference(void);
};
#endif
