/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionetyu;
    QAction *actione2e23;
    QAction *actione32e23e2;
    QAction *actione232e23e;
    QAction *actione2e32;
    QWidget *centralWidget;
    QPushButton *ClickMe;
    QPushButton *pushButton_2;
    QMenuBar *menuBar;
    QMenu *menu123456;
    QMenu *menuerty;
    QMenu *menue32e32e;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(436, 371);
        actionetyu = new QAction(MainWindow);
        actionetyu->setObjectName(QStringLiteral("actionetyu"));
        actione2e23 = new QAction(MainWindow);
        actione2e23->setObjectName(QStringLiteral("actione2e23"));
        actione32e23e2 = new QAction(MainWindow);
        actione32e23e2->setObjectName(QStringLiteral("actione32e23e2"));
        actione232e23e = new QAction(MainWindow);
        actione232e23e->setObjectName(QStringLiteral("actione232e23e"));
        actione2e32 = new QAction(MainWindow);
        actione2e32->setObjectName(QStringLiteral("actione2e32"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        ClickMe = new QPushButton(centralWidget);
        ClickMe->setObjectName(QStringLiteral("ClickMe"));
        ClickMe->setGeometry(QRect(40, 60, 131, 21));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ClickMe->sizePolicy().hasHeightForWidth());
        ClickMe->setSizePolicy(sizePolicy);
        ClickMe->setAcceptDrops(false);
        ClickMe->setIconSize(QSize(16, 20));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(40, 20, 131, 21));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 436, 20));
        menu123456 = new QMenu(menuBar);
        menu123456->setObjectName(QStringLiteral("menu123456"));
        menuerty = new QMenu(menuBar);
        menuerty->setObjectName(QStringLiteral("menuerty"));
        menue32e32e = new QMenu(menuerty);
        menue32e32e->setObjectName(QStringLiteral("menue32e32e"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menu123456->menuAction());
        menuBar->addAction(menuerty->menuAction());
        menuerty->addAction(actionetyu);
        menuerty->addSeparator();
        menuerty->addSeparator();
        menuerty->addSeparator();
        menuerty->addAction(menue32e32e->menuAction());
        menuerty->addAction(actione2e23);
        menuerty->addAction(actione32e23e2);
        menue32e32e->addSeparator();
        menue32e32e->addAction(actione232e23e);
        menue32e32e->addAction(actione2e32);

        retranslateUi(MainWindow);
        QObject::connect(ClickMe, SIGNAL(clicked()), MainWindow, SLOT(close()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        actionetyu->setText(QApplication::translate("MainWindow", "etyu", Q_NULLPTR));
        actione2e23->setText(QApplication::translate("MainWindow", "e2e23", Q_NULLPTR));
        actione32e23e2->setText(QApplication::translate("MainWindow", "e32e23e2", Q_NULLPTR));
        actione232e23e->setText(QApplication::translate("MainWindow", "e232e23e", Q_NULLPTR));
        actione2e32->setText(QApplication::translate("MainWindow", "e2e32", Q_NULLPTR));
        ClickMe->setText(QApplication::translate("MainWindow", "Exit", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("MainWindow", "warning!", Q_NULLPTR));
        menu123456->setTitle(QApplication::translate("MainWindow", "123456", Q_NULLPTR));
        menuerty->setTitle(QApplication::translate("MainWindow", "erty", Q_NULLPTR));
        menue32e32e->setTitle(QApplication::translate("MainWindow", "e32e32e", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
