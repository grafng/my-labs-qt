#include <QDebug>
#include <QFile>
#include <QByteArray>

int main(/*int argc, char *argv[]*/)
{

    QFile *inputFile = new QFile; //Динамически создаем объект для работы с файлом
    inputFile->setFileName("input.txt"); //Устанавливаем имя файла
    inputFile->open(QIODevice::ReadOnly); //Открываем файл для чтения

    char symbol=0;
    char trigger=0;
    float conclusion=0;

    int i=0;
    int t=0;
    int top=0;
    int count=0;
    int summa=0;
    int number1;
    int number2;


    // нахождение 1 и 2 го числа

    while (!inputFile->atEnd())
    {

        QByteArray line = inputFile->readLine();
        //длинна строки
        t=strlen(line)-2;
        for (i=0;i<t;i++,top++)
        {


            symbol=line[i];
            //вывод посимвольно
            printf("%c",symbol);

            if  ( symbol == '+' || symbol == '-' || symbol == '/' || symbol == '*' )
            {
                trigger=symbol;
                number1=summa;
                summa=0;
                top=0;
            }
            if ( symbol <= '9' && symbol >= '1' )
            {

                symbol=(symbol-48);
                count=pow(10,top);
                summa+=symbol*count;
            }


        }
    }
    number2=summa/10;
    summa=0;


    // реверсия найденых чисел

    int reversedNumber = 0, remainder;

    while(number1 != 0)
    {
        remainder = number1%10;
        reversedNumber = reversedNumber*10 + remainder;
        number1 /= 10;
    }
    number1=reversedNumber;


    reversedNumber = 0;

    while(number2 != 0)
    {
        remainder = number2%10;
        reversedNumber = reversedNumber*10 + remainder;
        number2 /= 10;
    }
    number2=reversedNumber;

    //вычесление

    if ( trigger == '+' ) conclusion=number1+number2;
    if ( trigger == '-' ) conclusion=number1-number2;
    if ( trigger == '/' ) conclusion=number1/number2;
    if ( trigger == '*' ) conclusion=number1*number2;

    printf ("  =  %.2f",conclusion);



    //    qDebug() << summa << endl;

    inputFile->close(); //Закрываем файл
    delete inputFile; //Удаляем объект







    return 0;
}











