#include <TPIDMainWindow.h>
#include <QPainter>
#include <QString>
#include <time.h>

TPIDMainWindow :: TPIDMainWindow(QWidget *parent) : QMainWindow(parent)
{
    DT = DT0;
    NA = NA0;
    X = X0;
    Y = Y0;
    Kpx = Kpx0;
    Kpy = Kpy0;
    Kix = Kix0;
    Kiy = Kiy0;
    Kdx = Kdx0;
    Kdy = Kdy0;
    Ix = 0;
    Iy = 0;
    Exprev = 0;
    Eyprev = 0;

    srand(time(0));

    ui.setupUi(this);

    ui.edtdt->setText(QString::number(DT));
    ui.edtNoiseA->setText(QString::number(NA));
    ui.edtKp->setText(QString::number(Kpx));
    ui.edtKi->setText(QString::number(Kix));
    ui.edtKd->setText(QString::number(Kdx));

    ConnectSignalsWithSlots();

    timer.setInterval(DT);
    timer.start();
}

TPIDMainWindow :: ~TPIDMainWindow()
{

}

void TPIDMainWindow :: ConnectSignalsWithSlots()
{
    QObject::connect(ui.btnSetParams, SIGNAL(clicked()), this, SLOT(OnBtnSetParamsClicked()));
    QObject::connect(&timer, SIGNAL(timeout()), this, SLOT(OnTimer()));
};

void TPIDMainWindow :: paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QPen pen = painter.pen();
    pen.setWidth(2);
    painter.setPen(pen);
    //painter.drawLine(x0, y0, x0 + x, y0 + y);
    painter.drawEllipse(CP, 200, 200);
    painter.drawEllipse(QPoint(CP.x() + X, CP.y() + Y), 10, 10);
};

void TPIDMainWindow :: OnTimer()
{
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    double dx, dy, Px, Dx, Py, Dy;

    Ex = X;// - CP.x();
    Px = Kpx * Ex;
    Ix = Ix + Kix * Ex;
    if (abs(Ix/Kix) > MAX_D) Ix = Ix/abs(Ix) * MAX_D * Kix;
    Dx = Kdx * (Ex - Exprev);
    Exprev = Ex;
    dx = Px + Ix + Dx;
    if (abs(dx) > MAX_D) dx = dx/abs(dx) * MAX_D;

    Ey = Y;// - CP.y();
    Py = Kpy * Ey;
    Iy = Iy + Kiy * Ey;
    if (abs(Iy/Kiy) > MAX_D) Iy = Iy/abs(Iy) * MAX_D * Kiy;
    Dy = Kdy * (Ey - Eyprev);
    Eyprev = Ey;
    dy = Py + Iy + Dy;
    if (abs(dy) > MAX_D) dy = dy/abs(dy) * MAX_D;

    X -= dx;
    Y -= dy;

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    if (dx >= 0)
    {
        ui.horizontalSlider_4->setSliderPosition(dx);
        ui.horizontalSlider_2->setSliderPosition(0);
    } else {
        ui.horizontalSlider_2->setSliderPosition(abs(dx));
        ui.horizontalSlider_4->setSliderPosition(0);
    }

    if (dy >= 0)
    {
        ui.verticalSlider_4->setSliderPosition(dy);
        ui.verticalSlider_2->setSliderPosition(0);
    } else {
        ui.verticalSlider_2->setSliderPosition(abs(dy));
        ui.verticalSlider_4->setSliderPosition(0);
    }

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    X = X - ui.horizontalSlider->sliderPosition() + ui.horizontalSlider_3->sliderPosition();
    Y = Y - ui.verticalSlider->sliderPosition() + ui.verticalSlider_3->sliderPosition();
    X += NA * rand() / RAND_MAX;
    Y += NA * rand() / RAND_MAX;

    this->repaint();
}

void TPIDMainWindow :: OnBtnSetParamsClicked()
{
    DT = ui.edtdt->text().toDouble();
    NA = ui.edtNoiseA->text().toDouble();
    Kpx = ui.edtKp->text().toDouble();
    Kpy = Kpx;
    Kix = ui.edtKi->text().toDouble();
    Kiy = Kiy;
    Kdx = ui.edtKd->text().toDouble();
    Kdy = Kdx;
    Ix = 0;
    Iy = 0;
    Exprev = 0;
    Eyprev = 0;
}
