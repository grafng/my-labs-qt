#ifndef TPIDMAINWINDOW_H
#define TPIDMAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include <QTimer>
#include <ui_TPIDMainWindow.h>

//Формула ПИД-регулятора, используемая для расчета
//u(t) = P (t) + I (t) + D (t);
//P (t) = Kp * e (t);
//I (t) = I (t — 1) + Ki * e (t);
//D (t) = Kd * (e (t) — e (t — 1));


static const double   DT0 = 500,
NA0 = 5.,
X0 = 100.,
Y0 = 100.,
Kpx0 = 1.,
Kpy0 = 1.,
Kix0 = 1.,
Kiy0 = 1.,
Kdx0 = 0,
Kdy0 = 0,
MAX_D = 50;
static const QPoint CP = QPoint(400, 400);

class TPIDMainWindow : public QMainWindow {
    Q_OBJECT
private:
    double DT, NA, X, Y, Ex, Exprev, Ey, Eyprev, Ix, Iy, Kpx, Kpy, Kix, Kiy, Kdx, Kdy;

    Ui::TPIDMainWindowClass ui;
    QTimer timer;

    void ConnectSignalsWithSlots();       //Соединяем сигналы со слотами
    void paintEvent(QPaintEvent *event);  //Процедура отрисовки

public:
    TPIDMainWindow(QWidget *parent = 0);
    ~TPIDMainWindow();

public slots:
    void OnBtnSetParamsClicked();
    void OnTimer();
};

#endif // TPIDMAINWINDOW_H
