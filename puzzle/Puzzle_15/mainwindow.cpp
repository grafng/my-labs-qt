#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"
#include "form.h"
#include "ui_form.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_4_clicked() // Выход.
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this,"Exit", "Do you really want to leave the game?", QMessageBox::Yes | QMessageBox::No);
    if(reply == QMessageBox::Yes)
    {
        close();
    }
}

void MainWindow::on_pushButton_clicked() // Старт.
{
    Form *f = new Form();
    f->show();
    this->close();
}

void MainWindow::on_pushButton_3_clicked() // Информация.
{
     QMessageBox::information(this, "About",
                              "Course project - Puzzle 15."
                              "\nCompleted:"
                              "\n2nd year student of the group E-53"
                              "\nDydyshko Ilya Anatolievich."
                              "\nPress Enter to return to the menu...");
}

void MainWindow::on_pushButton_2_clicked() // Помощи.
{
    QMessageBox::information(this, "Help",
                             "To exit the game press the sad smiley."
                             "\nTo get information about the author then the surprised smiles."
                             "\nTo start the game, click on the smiley face steep."
                             "\nControl is via the keyboard arrows."
                             "\nTo complete the game you need to collect field Puzzle 15."
                             "\nPress Enter to return to the menu..."
                             );
}
