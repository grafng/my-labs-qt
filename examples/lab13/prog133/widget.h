#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QLabel>
#include <QMouseEvent>

class Widget : public QLabel
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

protected:
    virtual void mousePressEvent  (QMouseEvent* event);
    virtual void mouseReleaseEvent(QMouseEvent* event);
    virtual void mouseMoveEvent   (QMouseEvent* event);

    void    dumpEvent     (QMouseEvent* event, const QString& strMessage);
    QString modifiersInfo (QMouseEvent* event                           );
    QString buttonsInfo   (QMouseEvent* event                           );
};

#endif // WIDGET_H
