#include "widget.h"

Widget::Widget(QWidget *parent) : QLabel(parent)
{
    setAlignment(Qt::AlignCenter);
    setText("Mouse interactions\n(Press a mouse button)");
}


Widget::~Widget()
{

}

void Widget::mousePressEvent(QMouseEvent* event)
{
    dumpEvent(event, "Mouse Pressed");
}

void Widget::mouseReleaseEvent(QMouseEvent* event)
{
    dumpEvent(event, "Mouse Released");
}

void Widget::mouseMoveEvent(QMouseEvent* event)
{
    dumpEvent(event, "Mouse Is Moving");
}


void Widget::dumpEvent(QMouseEvent* event, const QString& strMsg)
{
    setText(strMsg
            + "\n buttons()=" + buttonsInfo(event)
            + "\n x()=" + QString::number(event->x())
            + "\n y()=" + QString::number(event->y())
            + "\n globalX()=" + QString::number(event->globalX())
            + "\n globalY()=" + QString::number(event->globalY())
            + "\n modifiers()=" + modifiersInfo(event)
           );
}


QString Widget::modifiersInfo(QMouseEvent* event)
{
    QString strModifiers;

    if(event->modifiers() & Qt::ShiftModifier) {
        strModifiers += "Shift ";
    }
    if(event->modifiers() & Qt::ControlModifier) {
        strModifiers += "Control ";
    }
    if(event->modifiers() & Qt::AltModifier) {
        strModifiers += "Alt";
    }
    return strModifiers;
}


QString Widget::buttonsInfo(QMouseEvent* event)
{
    QString strButtons;

    if(event->buttons() & Qt::LeftButton) {
        strButtons += "Left ";
    }
    if(event->buttons() & Qt::RightButton) {
        strButtons += "Right ";
    }
    if(event->buttons() & Qt::MidButton) {
        strButtons += "Middle";
    }
    return strButtons;
}
