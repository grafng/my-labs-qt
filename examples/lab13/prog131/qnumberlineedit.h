#ifndef QNUMBERLINEEDIT_H
#define QNUMBERLINEEDIT_H
#include <QLineEdit>
#include <QDebug>
#include <QKeyEvent>

class QNUmberLineEdit : public QLineEdit
{
public:
    QNUmberLineEdit(QWidget *parent);

private:
    virtual void keyPressEvent(QKeyEvent *event);
};

#endif // QNUMBERLINEEDIT_H
