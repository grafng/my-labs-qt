#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QDebug>
#include <QPaintEvent>
#include <QPainter>
#include <QRect>

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

private:
    void paintEvent(QPaintEvent *event);
};

#endif // WIDGET_H
