#include "widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    increment = new Increment;
    connect(increment,SIGNAL(doIt()),this,SLOT(slotDoIt()));
    mainLayout = new QVBoxLayout(this);
    lcd = new QLCDNumber(this);
    mainLayout->addWidget(lcd);
    slider = new QSlider(Qt::Horizontal,this);
    slider->setMinimum(0);
    slider->setMaximum(100);
    connect(slider,SIGNAL(valueChanged(int)),this,SLOT(valueChanged(int)));
    mainLayout->addWidget(slider);

    layoutButton = new QHBoxLayout;
    layoutButton->setMargin(0);
    mainLayout->addLayout(layoutButton);

    buttonStart = new QPushButton(tr("Start"),this);
    connect(buttonStart,SIGNAL(pressed()),this,SLOT(slotPushStart()));
    layoutButton->addWidget(buttonStart);

    buttonStop = new QPushButton(tr("Stop"),this);
    connect(buttonStop,SIGNAL(pressed()),this,SLOT(slotPushStop()));
    layoutButton->addWidget(buttonStop);
}

Widget::~Widget()
{
    delete increment;
}

void Widget::slotPushStart()
{
    increment->start();
}

void Widget::slotPushStop()
{
    increment->stop();
}

void Widget::slotDoIt()
{
    int a = lcd->intValue();
    a++;
    slider->setValue(a);
    lcd->display(a);
}

void Widget::valueChanged(int a)
{
    lcd->display(a);
}
