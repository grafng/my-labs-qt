#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLCDNumber>
#include <QSlider>
#include <QPushButton>
#include "increment.h"

class Widget : public QWidget
{
    Q_OBJECT
private:
    QVBoxLayout *mainLayout;
    QLCDNumber *lcd;
    QSlider *slider;
    QHBoxLayout *layoutButton;
    QPushButton *buttonStart;
    QPushButton *buttonStop;
    Increment *increment;

public:
    Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void slotPushStart();
    void slotPushStop();
    void slotDoIt();
    void valueChanged(int a);
};

#endif // WIDGET_H
