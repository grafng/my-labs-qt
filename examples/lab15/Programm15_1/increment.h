#ifndef INCREMENT_H
#define INCREMENT_H

#include <QObject>
#include <QThread>

class Increment : public QObject
{
    Q_OBJECT
public:
    explicit Increment(QObject *parent = 0);
    void start();
    void stop();

private:
    bool enableWork;
    void work();
signals:
    void doIt();

public slots:
};

#endif // INCREMENT_H
