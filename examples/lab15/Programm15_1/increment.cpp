#include "increment.h"

Increment::Increment(QObject *parent) : QObject(parent)
{
    enableWork = false;
}

void Increment::start()
{
    enableWork = true;
    work();
}

void Increment::stop()
{
    enableWork = false;
}

void Increment::work()
{
    while (enableWork)
    {
        QThread::msleep(1000);
        emit doIt();
    }
}
