#include "increment.h"

Increment::Increment(QObject *parent) : QThread(parent)
{
    enableWork = false;
}

void Increment::startWork()
{
    enableWork = true;
    start();
}

 void Increment::run()
 {
     enableWork = true;
     work();
 }

void Increment::stop()
{
    enableWork = false;
    terminate();
}

void Increment::work()
{
    while (enableWork)
    {
        QThread::msleep(1000);
        emit doIt();
    }
}
