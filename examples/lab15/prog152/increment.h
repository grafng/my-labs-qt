#ifndef INCREMENT_H
#define INCREMENT_H

#include <QObject>
#include <QThread>

class Increment : public QThread
{
    Q_OBJECT
public:
    explicit Increment(QObject *parent = 0);
    void startWork();
    void stop();

private:
    bool enableWork;
    void work();
    void run();

signals:
    void doIt();

public slots:
};

#endif // INCREMENT_H
