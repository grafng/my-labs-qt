#include "widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    mainLayout = new QGridLayout(this);

    labelOpenFile = new QLabel(tr("Open file"),this);
    mainLayout->addWidget(labelOpenFile,0,0,1,1);
    editOpenFile = new QLineEdit(this);
    editOpenFile->setReadOnly(true);
    mainLayout->addWidget(editOpenFile,0,1,1,1);
    buttonOpenFile = new QPushButton(tr("Open file"),this);
    connect(buttonOpenFile,SIGNAL(pressed()),this,SLOT(slotPushFile()));
    mainLayout->addWidget(buttonOpenFile,0,2,1,1);

    labelSaveFile = new QLabel(tr("Save file"),this);
    mainLayout->addWidget(labelSaveFile,1,0,1,1);
    editSaveFile = new QLineEdit(this);
    editSaveFile->setReadOnly(true);
    mainLayout->addWidget(editSaveFile,1,1,1,1);
    buttonSaveFile = new QPushButton(tr("Save file"),this);
    connect(buttonSaveFile,SIGNAL(pressed()),this,SLOT(slotSaveFile()));
    mainLayout->addWidget(buttonSaveFile,1,2,1,1);

    labelDir = new QLabel(tr("Dir"),this);
    mainLayout->addWidget(labelDir,2,0,1,1);
    editDir = new QLineEdit(this);
    editDir->setReadOnly(true);
    mainLayout->addWidget(editDir,2,1,1,1);
    buttonDir = new QPushButton(tr("Dir"),this);
    connect(buttonDir,SIGNAL(clicked(bool)),this,SLOT(slotPushDir()));
    mainLayout->addWidget(buttonDir,2,2,1,1);
}

Widget::~Widget()
{

}

void Widget::slotPushFile()
{
    editOpenFile->setText(QFileDialog::getOpenFileName(this,tr("Open File"),""));
}

void Widget::slotSaveFile()
{
    editSaveFile->setText(QFileDialog::getSaveFileName(this,tr("Save File"),""));
}

void Widget::slotPushDir()
{
    editDir->
setText(QFileDialog::getExistingDirectory(this,tr("Directory"),""));
}
