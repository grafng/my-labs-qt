#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QFileDialog>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

private:
    QGridLayout *mainLayout;
    QLabel *labelOpenFile;
    QLineEdit *editOpenFile;
    QPushButton *buttonOpenFile;
    QLabel *labelSaveFile;
    QLineEdit *editSaveFile;
    QPushButton *buttonSaveFile;
    QLabel *labelDir;
    QLineEdit *editDir;
    QPushButton *buttonDir;


private slots:
    void slotPushFile();
    void slotSaveFile();
    void slotPushDir();
};

#endif // WIDGET_H
