QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Programm9_2
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h
