#include <QLabel>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv); //Передаем классу QApplication парамтры запуска
    QLabel w; //Создам виджет верхнего уровня
    w.setText("Hello world"); //Устанавливаем текс
    w.show(); //Делаем виджет видимым

    return a.exec();
}
