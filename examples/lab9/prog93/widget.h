#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QDebug>

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

private:
    QPushButton *button=NULL;
    quint16 pressCount=0;

private slots:
    void slotPressButton();
};

#endif // WIDGET_H
