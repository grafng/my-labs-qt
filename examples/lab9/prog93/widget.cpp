#include "widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    button = new QPushButton(tr("Press me"),this);
    connect(button,SIGNAL(pressed()),this,SLOT(slotPressButton()));
}

Widget::~Widget()
{

}

void Widget::slotPressButton()
{
    qDebug()<<"Button pressed "<<QString::number(pressCount);
    pressCount++;
}
