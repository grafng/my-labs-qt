#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QToolBar>
#include <QAction>
#include <QMenuBar>
#include <QStatusBar>
#include <QMdiArea>
#include <QDebug>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    QMdiArea*      m_pma;
    QVBoxLayout *mainLayout;
    QLabel *labelText;
    QToolBar *toolBar;

    QAction *actionNew;
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionHelp;
    QAction *actionExit;

    QAction *actionCascade;
    QAction *actionTile;

private slots:
    void slotNew();
};

#endif // MAINWINDOW_H
