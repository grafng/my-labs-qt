#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    m_pma = new QMdiArea;
    m_pma->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    m_pma->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setCentralWidget(m_pma);



    toolBar = new QToolBar(this);
    toolBar->setIconSize(QSize(48,48));
    addToolBar(toolBar);

    actionNew = new QAction(QIcon(":/icon/new.png"),tr("New"));
    actionNew->setShortcut(Qt::CTRL+Qt::Key_N);
    connect(actionNew,SIGNAL(triggered(bool)),this,SLOT(slotNew()));
    toolBar->addAction(actionNew);

    actionOpen = new QAction(QIcon(":/icon/open.png"),tr("Open"));
    actionOpen->setShortcut(Qt::CTRL+Qt::Key_O);
    toolBar->addAction(actionOpen);

    actionSave = new QAction(QIcon(":/icon/save.png"),tr("Save"));
    actionSave->setShortcut(Qt::CTRL+Qt::Key_S);
    toolBar->addAction(actionSave);

    actionHelp = new QAction(QIcon(":/icon/help.png"),tr("Help"));
    toolBar->addAction(actionHelp);

    actionExit = new QAction(QIcon(":/icon/exit.png"),tr("Exit"));
    actionExit->setShortcut(Qt::ALT+Qt::Key_X);
    connect(actionExit,SIGNAL(triggered(bool)),this,SLOT(close()));
    toolBar->addAction(actionExit);

    actionCascade = new QAction(QIcon(":/icon/cascade.png"),tr("Cascade"));

connect(actionCascade,SIGNAL(triggered(bool)),m_pma,SLOT(cascadeSubWindows()))
;
    toolBar->addAction(actionCascade);

    actionTile = new QAction(QIcon(":/icon/tile.png"),tr("Tile"));
    connect(actionTile,SIGNAL(triggered(bool)),m_pma,SLOT(tileSubWindows()));
    toolBar->addAction(actionTile);

    QMenu *menu = menuBar()->addMenu(tr("&File"));

    menu->addAction(actionOpen);
    menu->addAction(actionSave);
    menu->addAction(actionExit);

    menu = menuBar()->addMenu(tr("&Window"));

    menu->addAction(actionTile);
    menu->addAction(actionCascade);

    menu = menuBar()->addMenu(tr("&Help"));
    menu->addAction(actionHelp);

    statusBar()->addWidget(new QLabel(tr("Status bar")));

}

MainWindow::~MainWindow()
{

}

void MainWindow::slotNew()
{
    qDebug()<<"New";
    QLabel *labelText = new QLabel(tr("SubWindow"),m_pma);
    m_pma->addSubWindow(labelText);
    labelText->show();
}
